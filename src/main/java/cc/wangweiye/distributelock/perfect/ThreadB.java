package cc.wangweiye.distributelock.perfect;


public class ThreadB extends Thread {
    private Service2 service2;

    public ThreadB(Service2 service2) {
        this.service2 = service2;
    }

    @Override
    public void run() {
        service2.seckill();
    }
}